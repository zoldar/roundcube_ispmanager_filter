(function () {
    var R = React;
    var D = React.DOM;
    var cmd = function () {
        return window.ispmanager_rule.cmd;
    };
    var utils = window.ispmanager_rule.utils;
    var ui = window.ispmanager_rule.components;
    var getLabel = utils.getLabel;
    var capitalize = utils.capitalize;
    var ImmutableRenderMixin = utils.ImmutableRenderMixin;

    /*
     * Components
     */

    ui.ListButton = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return D.a({
                title: this.props.title,
                className: "button " + this.props.className,
                role: "button",
                tabIndex: 0,
                href: "#",
                ariaDisabled: false,
                onClick: this.props.onClick,
                onBlur: this.props.onBlur
            }, "");
        }
    });

    ui.RuleListRow = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;
            var classes = "";

            if (!this.props.data.get('active')) {
                classes += " disabled";
            }

            if (this.props.data.get('selected')) {
                classes += " selected focused";
            }

            return (
                // class names: selected focused / disabled
                // for selected, tabIndex: 0 in td
                D.tr({
                        className: classes,
                        ariaSelected: false,
                        onClick: function (e) {
                            e.preventDefault();
                            cmd().selectRule(that.props.data.get('name'));
                        }
                    },
                    D.td({className: "name"}, this.props.data.get('name'))
                )
            );
        }
    });

    ui.RuleList = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;
            return (
                D.table({
                        id: "filterslist",
                        className: "records-table",
                        summary: getLabel("filterslist"),
                        role: "listbox"
                    },
                    D.tbody(null,
                        this.props.data.get("rules").map(function (rule) {
                            if (rule.get("name") == that.props.data.get("selectedRule")) {
                                rule = rule.set("selected", true);
                            }
                            return R.createElement(ui.RuleListRow, {data: rule})
                        })
                    ))
            );
        }
    });

    ui.RuleListPane = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({id: "filtersscreen"},
                    D.div({id: "filterslistbox", style: {width: "250px"}},
                        [
                            D.div({className: "boxtitle"}, capitalize(getLabel('filters'))),
                            D.div(
                                {className: "boxlistcontent"},
                                R.createElement(ui.RuleList, this.props)
                            ),
                            D.div({className: "boxfooter"},
                                R.createElement(ui.ListButton, {
                                    title: getLabel('addfilter'),
                                    className: "add",
                                    content: "+",
                                    onClick: function (e) {
                                        e.preventDefault();
                                        cmd().newRule();
                                    }
                                }),
                                R.createElement(ui.ListButton, {
                                    title: getLabel('moreactions'),
                                    className: "groupactions",
                                    content: "&#9881;",
                                    onClick: function (e) {
                                        cmd().toggleListMenu(e.target);
                                    },
                                    onBlur: function (e) {
                                        // toggle menu back with a delay
                                        // to allow for click on menu item
                                        // to be handled
                                        window.setTimeout(
                                            function () {
                                                cmd().toggleListMenu(e.target);
                                            },
                                            300
                                        );
                                    }
                                })
                            )
                        ]
                    )
                )

            );
        }
    });

    ui.EmptyRulePane = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({
                    id: "filter-box",
                    className: "boxcontent",
                    style: {left: "260px"}
                }, getLabel('choosefiltertoedit'))
            );
        }
    });

    ui.RulePane = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({
                    id: "filter-box",
                    className: "boxcontent",
                    style: {padding: 0, left: "260px", overflowY: "scroll"}
                }, R.createElement(ui.RuleEditForm, this.props))
            );
        }
    });

    ui.EditConditionFormButtons = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var saveLabel;
            if (this.props.data.get('id')) {
                saveLabel = capitalize(getLabel('save'));
            } else {
                saveLabel = capitalize(getLabel('add'));
            }

            return (
                D.div({className: "buttons"}, [
                        D.input({
                            type: "button",
                            value: saveLabel,
                            className: "button mainaction",
                            onClick: function (e) {
                                e.preventDefault();
                                cmd().persistCurrentCondition();
                            }
                        }),
                        D.button({
                            onClick: function (e) {
                                e.preventDefault();
                                cmd().clearCurrentCondition();
                            }
                        }, capitalize(getLabel('close')))
                    ]
                )
            );
        }
    });

    ui.RuleListMenu = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var active = this.props.data.getIn(['currentRule', 'active']);
            var hasRule = this.props.data.getIn(['currentRule', 'id']);

            return (
                D.div({
                    id: "filtersetmenu",
                    className: "popupmenu",
                    ariaHidden: !this.props.data.get('show'),
                    style: {
                        display: this.props.data.get('show') ? "block" : "none",
                        left: this.props.data.get("left") - 25,
                        top: this.props.data.get("top") - 130
                    }
                }, [
                    D.ul(null, [
                        D.li(null,
                            D.a({
                                    role: "button",
                                    href: "#",
                                    className: hasRule ? "active" : "",
                                    tabIndex: hasRule ? 0 : -1,
                                    ariaDisabled: !hasRule,
                                    onClick: function (e) {
                                        e.preventDefault();
                                        cmd().toggleCurrentRule();
                                    }
                                },
                                active ? capitalize(getLabel('suspend')) : capitalize(getLabel('resume')))
                        ),
                        D.li(null,
                            D.a({
                                    role: "button",
                                    href: "#",
                                    className: hasRule ? "active" : "",
                                    tabIndex: hasRule ? 0 : -1,
                                    ariaDisabled: !hasRule,
                                    onClick: function (e) {
                                        e.preventDefault();
                                        cmd().deleteCurrentRule();
                                    }
                                },
                                capitalize(getLabel('delete')))
                        )
                    ])
                ])
            );
        }
    });

})();
