"use strict";

/*
 * For localization and templating purposes. Proper JS files
 * are included separately.
 */
window.ispmanager_rule = {
    labels: {},
    components: {},
    cmd: {}
};

/*
 * Common utilities used across the componenets and UI
 */
window.ispmanager_rule.utils = {
    capitalize: function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },
    getLabel: function (label, params) {
        params = params || {};
        var labels = window.ispmanager_rule.labels;
        var output = label;

        if (labels.hasOwnProperty(label)) {
            output = labels[label];
        }

        for (var param in params) {
            if (params.hasOwnProperty(param)) {
                output = output.replace('{' + param + '}', params[param]);
            }
        }

        return output;
    },
    // Code taken from https://github.com/jurassix/react-immutable-render-mixin
    ImmutableRenderMixin: (function () {
        function shallowEqualImmutable(objA, objB) {
            if (objA === objB || Immutable.is(objA, objB)) {
                return true;
            }

            if (typeof objA !== 'object' || objA === null ||
                typeof objB !== 'object' || objB === null) {
                return false;
            }

            var keysA = Object.keys(objA);
            var keysB = Object.keys(objB);

            if (keysA.length !== keysB.length) {
                return false;
            }

            // Test for A's keys different from B.
            var bHasOwnProperty = Object.prototype.hasOwnProperty.bind(objB);
            for (var i = 0; i < keysA.length; i++) {
                if (!bHasOwnProperty(keysA[i])
                    || !Immutable.is(objA[keysA[i]], objB[keysA[i]])) {
                    return false;
                }
            }

            return true;
        }

        return {
            shouldComponentUpdate: function (nextProps, nextState) {
                return !shallowEqualImmutable(this.props, nextProps)
                    || !shallowEqualImmutable(this.state, nextState);
            }
        }
    })()
};

/*


 */
window.IspmanagerRuleUI = function (rcube_webmail, rcmail, containerId) {
    /*
     * App state
     */
    var State = Immutable.fromJS({
        rules: [],
        listMenu: {show: false, top: 0, left: 0},
        selectedRule: null,
        currentRule: {
            currentAction: {},
            currentCondition: {}
        }
    });

    /*
     * Helpers
     */

    var utils = window.ispmanager_rule.utils;

    function capitalize(string) {
        return utils.capitalize(string);
    }

    function getLabel(label, params) {
        return utils.getLabel(label, params);
    }

    function objPosition(obj) {
        var curleft = 0, curtop = 0;

        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
        }

        return [curleft, curtop];
    }

    function showConfirmPopup(content, title, actionLabel, action) {
        var buttons = {};

        buttons[actionLabel] = function (e) {
            action();
            popup.dialog('close');
        };

        buttons[capitalize(getLabel('cancel'))] = function (e) {
            popup.dialog('close');
        };

        var popup = rcmail.show_popup_dialog(
            content,
            title,
            buttons,
            {
                button_classes: ['mainaction'],
                modal: true,
                closeOnEscape: true,
                close: function (e, ui) {
                    rcmail.ksearch_hide();
                    $(this).remove();
                }
            }
        );
    }

    function asList(n) {
        if (n && !Immutable.List.isList(n)) {
            return Immutable.List.of(n);
        }

        if (n) {
            return n;
        }

        return Immutable.List();
    }

    /*
     * Entities
     */

    function extractChoices(input, field) {
        var field = input.find(function (entry) {
            return entry.get('$name') == field;
        }, Immutable.Map())

        if (field) {
            return field
                .get("val", Immutable.List())
                .map(function (choice) {
                    return Immutable.List.of(choice.get('$key'), choice.get('$'));
                });
        }

        return Immutable.List();
    }

    var fieldTranslations = {
        'actval_d': 'value',
        'folder': 'folder',
        'foldval': 'customFolder',
        'not_d': 'negation',
        'values': 'values',
        'mod': 'modifier',
        'params': 'parameters',
        'size_d': 'size'
    };

    function extractFieldsToHide(input, field) {
        return input
            .find(function (entry) {
                return entry.get('$name') == field
            }, Immutable.Map())
            .getIn(["select", 0, "if"], Immutable.List())
            .reduce(function (hideMap, entry) {
                var action = entry.get('$value');
                var hide = entry.get('$hide');

                return hideMap.updateIn([action], Immutable.Set(), function (action) {
                    return action.add(fieldTranslations[hide]);
                });
            }, Immutable.Map());
    }

    var Model = {};

    Model.Rule = function (input) {
        var allanyChoices = extractChoices(input.get("slist"), "condcomp");
        var positionChoices = extractChoices(input.get("slist"), "pos");

        return Immutable.fromJS({
            id: input.getIn(['id', '$']),
            name: input.getIn(['name', '$']),
            active: input.getIn(['active', '$']) == "on",
            allany: {
                value: input.getIn(['allany', '$']),
                meta: {choices: allanyChoices}
            },
            position: {
                value: input.getIn(['pos', '$']),
                meta: {choices: positionChoices}
            }
        });
    };

    Model.Action = function (input) {
        var typeChoices = extractChoices(input.get("slist"), "action");
        var folderChoices = extractChoices(input.get("slist"), "folder");
        var fieldsToHide = extractFieldsToHide(input.getIn(['metadata', 'form', 'field']), 'action_d');

        return Immutable.fromJS({
            meta: {hide: fieldsToHide},
            id: input.getIn(['elid', '$']),
            type: {
                value: input.getIn(['action', '$']),
                meta: {choices: typeChoices}
            },
            value: input.getIn(['actval', '$']),
            folder: {
                value: input.getIn(['folder', '$']),
                meta: {choices: folderChoices}
            },
            customFolder: ''
        });
    };

    Model.Condition = function (input) {
        var typeChoices = extractChoices(input.get("slist"), "what");
        var modifierChoices = extractChoices(input.get("slist"), "mod");
        var fieldsToHide = extractFieldsToHide(input.getIn(['metadata', 'form', 'field']), 'what');

        // per default, hide a given set of fields
        fieldsToHide = fieldsToHide.set('else', Immutable.Set.of('size', 'parameters'));

        return Immutable.fromJS({
            meta: {hide: fieldsToHide},
            id: input.getIn(['elid', '$']),
            type: {
                value: input.getIn(['what', '$']),
                meta: {choices: typeChoices}
            },
            negation: input.getIn(['ifnot', '$']) != 'off',
            modifier: {
                value: input.getIn(['mod', '$']),
                meta: {choices: modifierChoices}
            },
            values: input.getIn(['values', '$']),
            parameters: input.getIn(['params', '$']),
            size: input.getIn(['size', '$'])
        });
    };

    Model.RuleListItem = function (input) {
        return Immutable.fromJS({
            id: input.getIn(['id', '$']),
            name: input.getIn(['name', '$']),
            active: input.getIn(['active', '$']) == "on",
            allany: input.getIn(['allany', '$'])
        });
    };

    Model.ConditionListItem = function (input) {
        return Immutable.fromJS({
            id: input.getIn(['id', '$']),
            modifier: input.getIn(['mod', '$']),
            parameter: input.getIn(['param', '$']),
            type: input.getIn(['what', '$'])
        });
    };

    Model.ActionListItem = function (input) {
        return Immutable.fromJS({
            id: input.getIn(['id', '$']),
            type: input.getIn(['action', '$']),
            value: input.getIn(['actval', '$'])
        })
    };

    /*
     * Response dispatch and handling
     */

    var Handler = {};

    Handler.updateRulesList = function (response) {
        var entries = asList(response.get('elem')).map(function (entry) {
            return Model.RuleListItem(entry);
        });

        State = State.set('rules', entries);
        render();
    };

    Handler.updateRule = function (response) {
        State = State.updateIn(['currentRule'], function (rule) {
            return rule.merge(Model.Rule(response));
        });
        render();
    };

    Handler.addRuleSuccess = function (response) {
        Cmd.fetchRules();

        Cmd.selectLastRule();

        rcmail.display_message(getLabel('addednewfilter'), "confirmation");
    };

    Handler.updateRuleSuccess = function (response) {
        Cmd.fetchRules();
        rcmail.display_message(getLabel('savedchanges'), "confirmation");
    };

    Handler.updateConditionList = function (response) {
        var entries = asList(response.get("elem")).map(function (entry) {
            return Model.ConditionListItem(entry);
        });

        State = State.updateIn(["currentRule"], function (rule) {
            return rule.set("conditions", entries);
        });

        render();
    };

    Handler.updateActionList = function (response) {
        var entries = asList(response.get("elem")).map(function (entry) {
            return Model.ActionListItem(entry);
        });

        State = State.updateIn(["currentRule"], function (rule) {
            return rule.set("actions", entries);
        });

        render();
    };

    Handler.updateAction = function (response) {
        State = State.setIn(
            ['currentRule', 'currentAction'],
            Model.Action(response)
        );
        render();
    };

    Handler.updateActionSuccess = function (response) {
        Cmd.clearCurrentAction();
        Cmd.updateActionList();
        rcmail.display_message(getLabel('actionsaved'), "confirmation");
    };

    Handler.deleteActionSuccess = function (response) {
        Cmd.clearCurrentAction();
        Cmd.updateActionList();
        rcmail.display_message(getLabel('actiondeleted'), "confirmation");
    };

    Handler.updateCondition = function (response) {
        State = State.setIn(
            ['currentRule', 'currentCondition'],
            Model.Condition(response)
        );
        render();
    };

    Handler.updateConditionSuccess = function (response) {
        Cmd.clearCurrentCondition();
        Cmd.updateConditionList();
        rcmail.display_message(getLabel('conditionsaved'), "confirmation");
    };

    Handler.deleteConditionSuccess = function (response) {
        Cmd.clearCurrentCondition();
        Cmd.updateConditionList();
        rcmail.display_message(getLabel('conditiondeleted'), "confirmation");
    };

    Handler.deleteRuleSuccess = function (response) {
        var deletedId = response.getIn(['tparams', 'elid', '$']);
        var currentId = State.getIn(['currentRule', 'id']);

        if (deletedId == currentId) {
            Cmd.clearSelectedRule();
        }
        Cmd.fetchRules();
        rcmail.display_message(getLabel('filterdeleted'), "confirmation");
    };

    Handler.suspendRuleSuccess = function (response) {
        var currentId = State.getIn(['currentRule', 'id']);
        Cmd.fetchRules();
        Cmd.selectRule(currentId);
        rcmail.display_message(getLabel('filtersuspended'), "confirmation");
    };

    Handler.resumeRuleSuccess = function (response) {
        var currentId = State.getIn(['currentRule', 'id']);
        Cmd.fetchRules();
        Cmd.selectRule(currentId);
        rcmail.display_message(getLabel('filterresumed'), "confirmation");
    };

    Handler.defaultError = function (response) {
        rcmail.display_message(response.getIn(['error', 'msg', '$']), "warning");
    };

    // handlers mapping
    var handlers = {
        'email.sorter': Handler.updateRulesList,
        'email.sorter.edit': Handler.updateRule,
        'email.sorter.add': Handler.updateRule,
        'email.sorter.cond': Handler.updateConditionList,
        'email.sorter.action': Handler.updateActionList,
        'email.sorter.action.edit': Handler.updateAction,
        'email.sorter.cond.edit': Handler.updateCondition
    };

    var successHandlers = {
        'email.sorter.edit': Handler.updateRuleSuccess,
        // This is a special case, because rule add operation must
        // be realized with a parameter initiating wizard-like process
        // of creating a rule
        'addsorter': Handler.addRuleSuccess, // email.sorter.add
        'email.sorter.delete': Handler.deleteRuleSuccess,
        'email.sorter.suspend': Handler.suspendRuleSuccess,
        'email.sorter.resume': Handler.resumeRuleSuccess,
        'email.sorter.action.edit': Handler.updateActionSuccess,
        'email.sorter.action.delete': Handler.deleteActionSuccess,
        'email.sorter.cond.edit': Handler.updateConditionSuccess,
        'email.sorter.cond.delete': Handler.deleteConditionSuccess
    };

    var errorHandlers = {
        default: Handler.defaultError
    };

    // handlers dispatch entry point
    rcube_webmail.prototype.ispmanager_rule_handle_response = function (_, o) {
        console.log(o);

        var isOk = o.doc.ok;
        var isError = o.doc.error;
        // rule add operation is initiated through a wizard-like interface
        var isRuleAddOk = o.doc.tparams && o.doc.tparams.func.$ == 'email.sorter.cond.add' && o.doc.steps;

        if (isOk || isRuleAddOk) {
            successHandlers[o.doc.tparams.func.$](Immutable.fromJS(o.doc));
        } else if (isError) {
            // FIXME: better error handling TBD
            var action = 'default';
            errorHandlers[action](Immutable.fromJS(o.doc));
        } else {
            handlers[o.doc.tparams.func.$](Immutable.fromJS(o.doc));
        }
    };

    /*
     * Commands
     */

    // Low-level API call
    function apiCall(action, params) {
        params['func'] = action;

        rcmail.http_post(
            'plugin.ispmanager_rule-action',
            '_params=' + encodeURIComponent(JSON.stringify(params))
        );
    }

    var Cmd = {};

    Cmd.toggleListMenu = function (button) {
        var left = 0, top = 0;

        if (button) {
            var position = objPosition(button);
            left = position[0];
            top = position[1];
        }

        State = State.update('listMenu', function (menu) {
            return Immutable.fromJS({
                show: !menu.get('show'),
                left: left,
                top: top
            });
        });

        render();
    };

    Cmd.fetchRules = function () {
        apiCall('email.sorter', {elid: '_USER'});
    };

    Cmd.selectRule = function (id) {
        State = State.setIn(['currentRule', 'currentAction'], Immutable.Map());
        State = State.setIn(['currentRule', 'currentCondition'], Immutable.Map());
        State = State.set('selectedRule', id);

        render();

        apiCall('email.sorter.edit', {plid: '_USER', elid: id});
        apiCall('email.sorter.action', {plid: '_USER', elid: id});
        apiCall('email.sorter.cond', {plid: '_USER', elid: id});
    };

    Cmd.selectLastRule = function() {
        Cmd.clearSelectedRule();
    };

    Cmd.clearSelectedRule = function () {
        State = State.set('currentRule', Immutable.fromJS(
            {currentAction: {}, currentCondition: {}}
        ));
        State = State.set('selectedRule', null);

        render();
    };

    Cmd.updateConditionList = function () {
        apiCall('email.sorter.cond', {plid: '_USER', elid: State.getIn(['currentRule', 'id'])});
    };

    Cmd.updateActionList = function () {
        apiCall('email.sorter.action', {plid: '_USER', elid: State.getIn(['currentRule', 'id'])});
    };

    Cmd.updateAllany = function (value) {
        State = State.setIn(["currentRule", "allany", "value"], value);
        render();
    };

    Cmd.updatePlaceBefore = function (value) {
        State = State.setIn(["currentRule", "position", "value"], value);
        render();
    };

    Cmd.updateActionType = function (type) {
        State = State.setIn(["currentRule", "currentAction", "type", "value"], type);
        render();
    };

    Cmd.updateRuleActive = function (active) {
        State = State.setIn(["currentRule", "active"], active);
        render();
    };

    Cmd.updateActionValue = function (value) {
        State = State.setIn(["currentRule", "currentAction", "value"], value);
        render();
    };

    Cmd.updateActionFolder = function (folder) {
        State = State.setIn(["currentRule", "currentAction", "folder", "value"], folder);
        render();
    };

    Cmd.updateActionCustomFolder = function (folder) {
        State = State.setIn(["currentRule", "currentAction", "customFolder"], folder);
        render();
    };

    Cmd.persistCurrentAction = function () {
        var ruleName = State.getIn(['currentRule', 'name']);
        var currentAction = State.getIn(['currentRule', 'currentAction']);

        var elid = currentAction.get('id');
        var action = currentAction.getIn(['type', 'value']);
        var folder = currentAction.getIn(['folder', 'value']);
        var foldval = currentAction.get('customFolder');
        var actval = currentAction.get('value');

        apiCall('email.sorter.action.edit', {
            sok: 'ok',
            plid: '_USER/' + ruleName,
            elid: elid,
            action: action,
            folder: folder,
            foldval: foldval,
            actval: actval
        });
    };

    Cmd.clearCurrentAction = function () {
        State = State.setIn(['currentRule', 'currentAction'], Immutable.Map())
        render();
    };

    Cmd.newRule = function () {
        apiCall('email.sorter.add', {
            plid: '_USER'
        });
    };

    Cmd.updateRuleName = function (name) {
        State = State.setIn(['currentRule', 'name'], name);
        render();
    };

    Cmd.addRule = function () {
        var currentRule = State.get("currentRule");

        var name = currentRule.get("name");
        var pos = currentRule.getIn(["position", "value"]);

        apiCall('email.sorter.action.add', {
            sok: 'ok',
            plid: '_USER',
            elid: '',
            name: name,
            pos: pos,
            condcomp: 'allof'
        });
    };

    Cmd.persistRule = function () {
        var currentRule = State.get("currentRule");

        var id = currentRule.get("id");
        var name = currentRule.get("name");
        var allany = currentRule.getIn(["allany", "value"]);
        var pos = currentRule.getIn(["position", "value"]);
        var active = currentRule.get("active") ? "on" : "off";

        apiCall('email.sorter.edit', {
            sok: 'ok',
            plid: '_USER',
            elid: id,
            name: name,
            condcomp: allany,
            pos: pos,
            active: active
        });
    };

    Cmd.selectAction = function (actionId) {
        apiCall('email.sorter.action.edit', {plid: '_USER', elid: actionId});
    };

    Cmd.newAction = function () {
        apiCall('email.sorter.action.edit', {plid: '_USER'});
    };

    Cmd.deleteAction = function (actionId) {
        var ruleId = State.getIn(['currentRule', 'id']);

        var deleteAction = function (e) {
            apiCall('email.sorter.action.delete', {plid: '_USER/' + ruleId, elid: actionId});
        };

        showConfirmPopup(
            getLabel('confirmactiondeletetext'),
            getLabel('confirmactiondeletecaption'),
            capitalize(getLabel('delete')),
            deleteAction
        );
    };

    Cmd.selectCondition = function (conditionId) {
        apiCall('email.sorter.cond.edit', {plid: '_USER', elid: conditionId});
    };

    Cmd.newCondition = function () {
        apiCall('email.sorter.cond.edit', {plid: '_USER'});
    };

    Cmd.deleteCondition = function (conditionId) {
        var ruleId = State.getIn(['currentRule', 'id']);

        var deleteAction = function (e) {
            apiCall('email.sorter.cond.delete', {plid: '_USER/' + ruleId, elid: conditionId});
        };

        showConfirmPopup(
            getLabel('confirmconditiondeletetext'),
            getLabel('confirmconditiondeletecaption'),
            capitalize(getLabel('delete')),
            deleteAction
        );
    };

    Cmd.updateConditionType = function (type) {
        State = State.setIn(['currentRule', 'currentCondition', 'type', 'value'], type);
        render();
    };

    Cmd.updateConditionNegation = function (negation) {
        State = State.setIn(['currentRule', 'currentCondition', 'negation'], negation);
        render();
    };

    Cmd.updateConditionModifier = function (modifier) {
        State = State.setIn(['currentRule', 'currentCondition', 'modifier', 'value'], modifier);
        render();
    };

    Cmd.updateConditionSize = function (size) {
        State = State.setIn(['currentRule', 'currentCondition', 'size'], size);
        render();
    };

    Cmd.updateConditionParameters = function (parameters) {
        State = State.setIn(['currentRule', 'currentCondition', 'parameters'], parameters);
        render();
    };

    Cmd.updateConditionValues = function (values) {
        State = State.setIn(['currentRule', 'currentCondition', 'values'], values);
        render();
    };

    Cmd.clearCurrentCondition = function () {
        State = State.setIn(['currentRule', 'currentCondition'], Immutable.Map());
        render();
    };

    Cmd.persistCurrentCondition = function () {
        var ruleName = State.getIn(['currentRule', 'name']);
        var currentCondition = State.getIn(['currentRule', 'currentCondition']);

        var elid = currentCondition.get('id');
        var what = currentCondition.getIn(['type', 'value']);
        var mod = currentCondition.getIn(['modifier', 'value']);
        var ifnot = currentCondition.get('negation') ? 'on' : 'off';
        var params = currentCondition.get('parameters');
        var values = currentCondition.get('values');
        var size = currentCondition.get('size');

        apiCall('email.sorter.cond.edit', {
            sok: 'ok',
            plid: '_USER/' + ruleName,
            elid: elid,
            what: what,
            mod: mod,
            ifnot: ifnot,
            params: params,
            values: values,
            size: size
        });
    };

    Cmd.deleteCurrentRule = function () {
        var currentRule = State.get("currentRule");
        var currentName = currentRule.get('name');
        var currentId = currentRule.get('id');

        if (currentRule.get('id')) {
            var deleteAction = function (e) {
                apiCall('email.sorter.delete', {
                    plid: '_USER',
                    elid: currentId
                });
            };

            showConfirmPopup(
                getLabel('confirmfilterdeletetext', {name: currentName}),
                getLabel('confirmfilterdeletecaption'),
                capitalize(getLabel('delete')),
                deleteAction
            );
        }
    };

    Cmd.toggleCurrentRule = function () {
        var currentRule = State.get("currentRule");
        var currentId = currentRule.get('id');

        var isActive = currentRule.get("active");

        if (currentRule.get('id')) {
            if (isActive) {
                apiCall('email.sorter.suspend', {
                    plid: '_USER',
                    elid: currentId
                });
            } else {
                apiCall('email.sorter.resume', {
                    plid: '_USER',
                    elid: currentId
                });
            }
        }
    };


    // Cmd is added to a public object
    // to be reachable for components
    window.ispmanager_rule.cmd = Cmd;

    /*
     * Rendering function
     */
    function render() {
        var ui = window.ispmanager_rule.components;

        React.render(
            React.createElement(ui.RulesView, {data: State}),
            document.getElementById(containerId)
        );
    }

    /*
     * Public API
     */
    return {
        init: function () {
            Cmd.fetchRules();
        }
    }
};

