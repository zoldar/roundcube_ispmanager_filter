<?php

class IspmanagerRule_Plugin extends PHPUnit_Framework_TestCase
{

    function setUp()
    {
        include_once __DIR__ . '/../ispmanager_rule.php';
    }

    /**
     * Plugin object construction test
     */
    function test_constructor()
    {
        $rcube  = rcube::get_instance();
        $plugin = new ispmanager_rule($rcube->api);

        $this->assertInstanceOf('ispmanager_rule', $plugin);
        $this->assertInstanceOf('rcube_plugin', $plugin);
    }
}

