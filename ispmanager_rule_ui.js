(function () {
    var R = React;
    var D = React.DOM;
    var cmd = function () {
        return window.ispmanager_rule.cmd;
    };
    var utils = window.ispmanager_rule.utils;
    var ui = window.ispmanager_rule.components;
    var getLabel = utils.getLabel;
    var capitalize = utils.capitalize;
    var ImmutableRenderMixin = utils.ImmutableRenderMixin;

    /*
     * Components
     */

    ui.ListButton = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return D.a({
                    title: this.props.title,
                    className: "listbutton " + this.props.className,
                    role: "button",
                    tabIndex: 0,
                    href: "#",
                    ariaDisabled: false,
                    onClick: this.props.onClick,
                    onBlur: this.props.onBlur
                },
                D.span({className: "inner"}, this.props.content)
            );
        }
    });

    ui.RuleListRow = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;
            var classes = "";

            if (!this.props.data.get('active')) {
                classes += " disabled";
            }

            if (this.props.data.get('selected')) {
                classes += " selected focused";
            }

            return (
                // class names: selected focused / disabled
                // for selected, tabIndex: 0 in td
                D.tr({
                        role: "option",
                        className: classes,
                        ariaSelected: false,
                        onClick: function (e) {
                            e.preventDefault();
                            cmd().selectRule(that.props.data.get('id'));
                        }
                    },
                    D.td({className: "name"}, this.props.data.get('name'))
                )
            );
        }
    });

    ui.RuleList = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;
            return (
                D.table({
                        id: "filterslist",
                        className: "listing focus",
                        summary: getLabel("filterslist"),
                        role: "listbox"
                    },
                    D.tbody(null,
                        this.props.data.get("rules").map(function (rule) {
                            if (rule.get("id") == that.props.data.get("selectedRule")) {
                                rule = rule.set("selected", true);
                            }
                            return R.createElement(ui.RuleListRow, {data: rule})
                        })
                    ))
            );
        }
    });

    ui.RuleListMenu = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var active = this.props.data.getIn(['currentRule', 'active']);
            var hasRule = this.props.data.getIn(['currentRule', 'id']);

            return (
                D.div({
                    id: "filtersetmenu",
                    className: "popupmenu",
                    ariaHidden: !this.props.data.get('show'),
                    style: {
                        display: this.props.data.get('show') ? "block" : "none",
                        left: this.props.data.get("left") - 220,
                        top: this.props.data.get("top") - 190
                    }
                }, [
                    D.h3({id: "aria-label-setactions", className: "voice"}, getLabel('actionsonfilters')),
                    D.ul({
                        className: "toolbarmenu",
                        id: "filtersetmenu-menu",
                        role: "menu",
                        ariaLabelledby: "aria-label-setactions"
                    }, [
                        D.li({role: "menuitem"},
                            D.a({
                                    role: "button",
                                    href: "#",
                                    className: hasRule ? "active" : "",
                                    tabIndex: hasRule ? 0 : -1,
                                    ariaDisabled: !hasRule,
                                    onClick: function (e) {
                                        e.preventDefault();
                                        cmd().toggleCurrentRule();
                                    }
                                },
                                active ? capitalize(getLabel('suspend')) : capitalize(getLabel('resume')))
                        ),
                        D.li({role: "menuitem"},
                            D.a({
                                    role: "button",
                                    href: "#",
                                    className: hasRule ? "active" : "",
                                    tabIndex: hasRule ? 0 : -1,
                                    ariaDisabled: !hasRule,
                                    onClick: function (e) {
                                        e.preventDefault();
                                        cmd().deleteCurrentRule();
                                    }
                                },
                                capitalize(getLabel('delete')))
                        )
                    ])
                ])
            );
        }
    });

    ui.RuleListPane = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({id: "filterslistbox", className: "uibox listbox", style: {width: "250px"}},
                    [
                        D.h2({className: "boxtitle"}, capitalize(getLabel('filters'))),
                        D.div(
                            {className: "scroller withfooter"},
                            R.createElement(ui.RuleList, this.props)
                        ),
                        D.div({className: "boxfooter"},
                            R.createElement(ui.ListButton, {
                                title: getLabel('addfilter'),
                                className: "add",
                                content: "+",
                                onClick: function (e) {
                                    e.preventDefault();
                                    cmd().newRule();
                                }
                            }),
                            R.createElement(ui.ListButton, {
                                title: getLabel('moreactions'),
                                className: "groupactions",
                                content: "&#9881;",
                                onClick: function (e) {
                                    cmd().toggleListMenu(e.target);
                                },
                                onBlur: function (e) {
                                    // toggle menu back with a delay
                                    // to allow for click on menu item
                                    // to be handled
                                    window.setTimeout(
                                        function () {
                                            cmd().toggleListMenu(e.target);
                                        },
                                        300
                                    );
                                }
                            })
                        )
                    ]
                )
            );
        }
    });

    ui.RuleNameReadOnlyField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({className: "filter-field"}, [
                    D.label({htmlFor: "_name"}, D.b(null, capitalize(getLabel('filtername')) + ": ")),
                    D.input({
                        name: "_name",
                        id: "_name",
                        value: this.props.data,
                        type: "text",
                        readOnly: true,
                        size: 30
                    })
                ])
            );
        }
    });

    ui.RuleNameField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({className: "filter-field"}, [
                    D.label({htmlFor: "_name"}, D.b(null, capitalize(getLabel('filtername')) + ": ")),
                    D.input({
                        name: "_name",
                        id: "_name",
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateRuleName(e.target.value);
                        },
                        size: 30,
                        type: "text"
                    })])
            );
        }
    });

    ui.AllAnyField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var currentValue = this.props.data.get("value");
            var choices = this.props.data.getIn(["meta", "choices"]);

            return (
                D.span({}, choices.map(function (entry, idx) {
                    var value = entry.get(0), label = entry.get(1);

                    return D.span({style: {paddingRight: "10px"}}, [
                        D.input({
                            id: "_allany" + idx,
                            name: "allany",
                            className: "radio",
                            type: "radio",
                            value: value,
                            checked: currentValue == value,
                            onClick: function (e) {
                                cmd().updateAllany(e.target.value);
                            }
                        }),
                        D.label({
                            htmlFor: "_allany" + idx
                        }, label)
                    ])
                }))
            );
        }
    });

    ui.PlaceBeforeField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({className: "filter-field", style: {paddingTop: "10px"}}, [
                    D.label({htmlFor: "_pos"}, capitalize(getLabel('placebefore')) + ": "),
                    D.select({
                            name: "_pos",
                            value: this.props.data.get("value"),
                            onChange: function (e) {
                                e.preventDefault();
                                cmd().updatePlaceBefore(e.target.value);
                            }
                        },
                        this.props.data.getIn(["meta", "choices"]).map(function (entry) {
                            return D.option({value: entry.get(0)}, entry.get(1));
                        })
                    )
                ])

            );
        }
    });

    ui.AddConditionButton = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.a({
                    href: "#",
                    title: capitalize(getLabel('addcondition')),
                    className: "button add",
                    onClick: function (e) {
                        e.preventDefault();
                        cmd().newCondition();
                    }
                }, "")
            );
        }
    });

    ui.ConditionTypeField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({htmlFor: "condition-type"}, capitalize(getLabel('conditiontype')) + ":"),
                    D.select({
                            id: "condition-type",
                            value: this.props.data.get('value'),
                            onChange: function (e) {
                                e.preventDefault();
                                cmd().updateConditionType(e.target.value);
                            }
                        },
                        this.props.data.getIn(['meta', 'choices']).map(function (entry) {
                            var value = entry.get(0), label = entry.get(1);

                            return D.option({value: value}, label);
                        })
                    )
                ])
            );
        }
    });

    ui.ConditionNegationField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.input({
                        id: "condition-negation",
                        type: "checkbox",
                        checked: this.props.data,
                        onClick: function (e) {
                            cmd().updateConditionNegation(e.target.checked);
                        }
                    }),
                    D.label({
                        htmlFor: "condition-negation",
                        style: {marginRight: "15px"}
                    }, getLabel('conditionnegation'))
                ])
            );
        }
    });

    ui.ConditionModifierField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({htmlFor: "condition-modifier"}, capitalize(getLabel('conditionmodifier')) + ":"),
                    this.props.prefix,
                    D.select({
                            id: "condition-modifier",
                            value: this.props.data.get('value'),
                            onChange: function (e) {
                                e.preventDefault();
                                cmd().updateConditionModifier(e.target.value);
                            }
                        },
                        this.props.data.getIn(['meta', 'choices']).map(function (entry) {
                                var value = entry.get(0), label = entry.get(1);

                                return D.option({value: value}, label);
                            }
                        ))
                ])
            );
        }
    });

    ui.ConditionSizeField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({htmlFor: "condition-size"}, capitalize(getLabel('conditionsize')) + ":"),
                    D.input({
                        id: "condition-size",
                        type: "text",
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateConditionSize(e.target.value);
                        }
                    })
                ])
            );
        }
    });

    ui.ConditionParametersField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({htmlFor: "condition-parameters"}, capitalize(getLabel('conditionparameters')) + ":"),
                    D.input({
                        id: "condition-parameters",
                        type: "text",
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateConditionParameters(e.target.value);
                        }
                    })
                ])
            );
        }
    });

    ui.ConditionValuesField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({htmlFor: "condition-values"}, capitalize(getLabel('conditionvalues')) + ":"),
                    D.textarea({
                        rows: 5,
                        cols: 50,
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateConditionValues(e.target.value);
                        }
                    })
                ])
            );
        }
    });

    ui.EditConditionFormButtons = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var saveLabel;
            if (this.props.data.get('id')) {
                saveLabel = capitalize(getLabel('save'));
            } else {
                saveLabel = capitalize(getLabel('add'));
            }

            return (
                D.div({className: "buttons"}, [
                        D.input({
                            type: "button",
                            value: saveLabel,
                            className: "button mainaction",
                            onClick: function (e) {
                                e.preventDefault();
                                cmd().persistCurrentCondition();
                            }
                        }),
                        D.button({
                            onClick: function (e) {
                                e.preventDefault();
                                cmd().clearCurrentCondition();
                            }
                        }, capitalize(getLabel('close')))
                    ]
                )
            );
        }
    });

    ui.EditConditionForm = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var fieldsToHide = this.props.data.getIn([
                'meta', 'hide', this.props.data.getIn(['type', 'value'])
            ], this.props.data.getIn(['meta', 'hide', 'else']));

            var negationField = null;
            if (!fieldsToHide.contains("negation")) {
                negationField = R.createElement(
                    ui.ConditionNegationField, {data: this.props.data.get('negation')}
                );
            }

            var modifierField = null;
            if (!fieldsToHide.contains("modifier")) {
                modifierField = D.div({className: "form-row"}, R.createElement(
                    ui.ConditionModifierField, {
                        prefix: negationField,
                        data: this.props.data.get('modifier')
                    }
                ));
            }

            var valuesField = null;
            if (!fieldsToHide.contains("values")) {
                valuesField = D.div({className: "form-row"}, R.createElement(
                    ui.ConditionValuesField, {data: this.props.data.get('values')}
                ));
            }

            var parametersField = null;
            if (!fieldsToHide.contains("parameters")) {
                parametersField = D.div({className: "form-row"},
                    R.createElement(ui.ConditionParametersField,
                        {data: this.props.data.get('parameters')})
                );
            }

            var sizeField = null;
            if (!fieldsToHide.contains("size")) {
                sizeField = D.div({className: "form-row"},
                    R.createElement(ui.ConditionSizeField, {data: this.props.data.get('size')})
                );
            }

            var saveLabel;
            if (this.props.data.get('id')) {
                saveLabel = capitalize(getLabel('save'));
            } else {
                saveLabel = capitalize(getLabel('add'));
            }

            return (
                D.div({className: "inline-edit-form"}, [
                    D.div({className: "form-row"}, R.createElement(ui.ConditionTypeField, {data: this.props.data.get('type')})),
                    parametersField,
                    modifierField,
                    valuesField,
                    sizeField,
                    R.createElement(ui.EditConditionFormButtons, this.props)
                ])
            );
        }
    });

    ui.DeleteConditionButton = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;

            return (
                D.a({
                    href: "#",
                    className: "button del",
                    onClick: function (e) {
                        e.preventDefault();
                        cmd().deleteCondition(that.props.data.get('id'));
                    }
                })
            );
        }
    });

    ui.EditConditionButton = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;
            return (
                D.input({
                    type: "button",
                    className: "button mainaction",
                    value: getLabel('edit'),
                    onClick: function (e) {
                        e.preventDefault();
                        cmd().selectCondition(that.props.data.get('id'));
                    }
                })
            );
        }
    });

    ui.ConditionTable = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.table({id: "condition-table", className: "filter-table", style: {width: "100%", textAlign: "left"}}, [
                    D.thead(null,
                        D.tr(null, [
                            D.th(null, capitalize(getLabel('conditiontypehead'))),
                            D.th(null, capitalize(getLabel('conditionmodifierhead'))),
                            D.th(null, capitalize(getLabel('conditionvalueshead'))),
                            D.th(null, "")
                        ])
                    ),
                    D.tbody(null,
                        this.props.data.map(function (condition) {
                            return D.tr(null, [
                                D.td(null, condition.get("type")),
                                D.td(null, condition.get("modifier")),
                                D.td(null, condition.get("parameter")),
                                D.td({className: "operations"}, [
                                    R.createElement(ui.EditConditionButton, {data: condition}),
                                    R.createElement(ui.DeleteConditionButton, {data: condition})
                                ])
                            ])
                        })
                    )
                ])
            );
        }
    });

    ui.ConditionsFieldSet = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var conditionsBlock = null;

            var editConditionForm = null;
            if (!this.props.data.get('currentCondition').isEmpty()) {
                editConditionForm = R.createElement(ui.EditConditionForm, {data: this.props.data.get('currentCondition')});
            }

            if (this.props.data.getIn(["allany", "value"]) != "nothing") {
                conditionsBlock = D.div(null, [
                    D.div({
                        style: {
                            textAlign: "right",
                            paddingTop: "8px"
                        }
                    }, R.createElement(ui.AddConditionButton, this.props)),
                    editConditionForm,
                    R.createElement(ui.ConditionTable, {data: this.props.data.get("conditions", Immutable.List())})
                ]);
            }

            return (
                D.fieldset({style: {marginTop: "20px"}}, [
                    D.legend(null, getLabel('inrelationtoincomingmail') + ":"),
                    D.div(null, R.createElement(ui.AllAnyField, {data: this.props.data.get("allany")})),
                    conditionsBlock
                ])
            );
        }
    });

    ui.ActionTypeField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({htmlFor: "action-type"}, capitalize(getLabel('actiontype')) + ":"),
                    D.select({
                            id: "action-type",
                            value: this.props.data.get('value'),
                            onChange: function (e) {
                                e.preventDefault();
                                cmd().updateActionType(e.target.value);
                            }
                        },
                        this.props.data.getIn(['meta', 'choices']).map(function (type) {
                            var value = type.get(0), label = type.get(1);

                            return D.option({value: value}, label);
                        })
                    )
                ])
            );
        }
    });

    ui.ValueActionParameter = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({values: "action-value"}, capitalize(getLabel('actionvalue')) + ":"),
                    D.input({
                        id: "action-value",
                        type: "text",
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateActionValue(e.target.value);
                        }
                    })
                ])
            );
        }
    });

    ui.FolderActionParameter = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({htmlFor: "action-folder"}, capitalize(getLabel('actionfolder')) + ":"),
                    D.select({
                        id: "action-folder",
                        value: this.props.data.get('value'),
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateActionFolder(e.target.value);
                        }
                    }, this.props.data.getIn(['meta', 'choices']).map(function (folder) {
                        var value = folder.get(0), label = folder.get(1);

                        return D.option({value: value}, label);
                    }))
                ])
            );
        }
    });

    ui.CustomFolderActionParameter = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({values: "action-custom-folder"}, capitalize(getLabel('actioncustomfolder')) + ":"),
                    D.input({
                        id: "action-custom-folder",
                        type: "text",
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateActionCustomFolder(e.target.value);
                        }
                    })
                ])
            );
        }
    });

    ui.EditActionForm = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var fieldsToHide = this.props.data.getIn(
                ['meta', 'hide', this.props.data.getIn(['type', 'value'])]
            );

            var valueParam = null;
            if (!fieldsToHide.contains('value')) {
                valueParam = D.div({className: "form-row"},
                    R.createElement(ui.ValueActionParameter, {data: this.props.data.get('value')})
                );
            }

            var folderParam = null;
            if (!fieldsToHide.contains('folder')) {
                folderParam = D.div({className: "form-row"},
                    R.createElement(
                        ui.FolderActionParameter,
                        {data: this.props.data.get('folder')}
                    )
                );
            }

            var customFolderParam = null;
            if (!fieldsToHide.contains('customFolder')
                && this.props.data.getIn(['folder', 'value']) == 'newfold') {
                customFolderParam = D.div({className: "form-row"},
                    R.createElement(
                        ui.CustomFolderActionParameter,
                        {data: this.props.data.get('customFolder')}
                    )
                );
            }

            var saveLabel;
            if (this.props.data.get('id')) {
                saveLabel = capitalize(getLabel('save'));
            } else {
                saveLabel = capitalize(getLabel('add'));
            }

            return (
                D.div({className: "inline-edit-form"}, [
                    D.div({className: "form-row"}, R.createElement(ui.ActionTypeField, {data: this.props.data.get('type')})),
                    valueParam,
                    folderParam,
                    customFolderParam,
                    D.div({className: "buttons"}, [
                            D.input({
                                type: "button",
                                value: saveLabel,
                                className: "button mainaction",
                                onClick: function (e) {
                                    e.preventDefault();
                                    cmd().persistCurrentAction();
                                }
                            }),
                            D.button({
                                onClick: function (e) {
                                    e.preventDefault();
                                    cmd().clearCurrentAction();
                                }
                            }, capitalize(getLabel('close')))
                        ]
                    )
                ])
            );
        }
    });

    ui.AddActionButton = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.a({
                    href: "#",
                    title: getLabel('addaction'),
                    className: "button add",
                    onClick: function (e) {
                        e.preventDefault();
                        cmd().newAction();
                    }
                }, "")
            );
        }
    });

    ui.DeleteActionButton = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;

            return (
                D.a({
                    href: "#",
                    className: "button del",
                    onClick: function (e) {
                        e.preventDefault();
                        cmd().deleteAction(that.props.data.get('id'));
                    }
                })
            );
        }
    });

    ui.EditActionButton = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;

            return (
                D.input({
                    type: "button",
                    className: "button mainaction",
                    value: getLabel('edit'),
                    onClick: function (e) {
                        e.preventDefault();
                        cmd().selectAction(that.props.data.get("id"));
                    }
                })
            );
        }
    });

    ui.ActionTable = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.table({id: "action-table", className: "filter-table", style: {width: "100%", textAlign: "left"}},
                    D.thead(null,
                        D.tr(null, [
                            D.th(null, capitalize(getLabel('actiontypehead'))),
                            D.th(null, capitalize(getLabel('actionvaluehead'))),
                            D.th(null, "")
                        ])
                    ),
                    D.tbody(null,
                        this.props.data.map(function (action) {
                            return D.tr(null, [
                                D.td(null, action.get("type")),
                                D.td(null, action.get("value")),
                                D.td({className: "operations"}, [
                                    R.createElement(ui.EditActionButton, {data: action}),
                                    R.createElement(ui.DeleteActionButton, {data: action})
                                ])
                            ]);
                        })
                    )
                )
            );
        }
    });

    ui.ActionsFieldSet = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var currentAction = this.props.data.get("currentAction");
            var addActionForm = null;

            if (!currentAction.isEmpty()) {
                addActionForm = R.createElement(ui.EditActionForm, {data: currentAction});
            }

            return (
                D.fieldset({style: {marginTop: "20px"}}, [
                    D.legend(null, "... " + getLabel('executefollowingactions') + ":"),
                    D.div({
                        style: {
                            textAlign: "right",
                            paddingTop: "8px"
                        }
                    }, R.createElement(ui.AddActionButton, this.props)),
                    addActionForm,
                    R.createElement(ui.ActionTable, {data: this.props.data.get("actions", Immutable.List())})
                ])
            );
        }
    });

    ui.RuleActiveField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span({id: "rule-active-field"}, [
                    D.label({htmlFor: "rule-active"}, [
                        D.input({
                            id: "rule-active",
                            type: "checkbox",
                            checked: this.props.data,
                            onClick: function (e) {
                                cmd().updateRuleActive(e.target.checked);
                            }
                        }),
                        capitalize(getLabel('enablefilter'))
                    ])
                ])
            );
        }
    });

    ui.RuleEditForm = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var editFields = null;
            var saveLabel = capitalize(getLabel('addfilter'));
            var saveCommand = cmd().addRule;
            var nameField = ui.RuleNameField;

            if (this.props.data.get('id')) {
                editFields = D.div(null, [
                    R.createElement(ui.ConditionsFieldSet, this.props),
                    R.createElement(ui.ActionsFieldSet, this.props)
                ]);
                saveLabel = capitalize(getLabel('savefilter'));
                saveCommand = cmd().persistRule;
                nameField = ui.RuleNameReadOnlyField;
            }

            return (
                D.div(null, [
                    D.div({id: "filter-title", className: "boxtitle"}, getLabel('filterdefinition')),
                    D.div({id: "filter-form", className: "boxcontent"},
                        D.form({name: "filterform"}, [
                            R.createElement(nameField, {data: this.props.data.get("name")}),
                            R.createElement(ui.PlaceBeforeField, {data: this.props.data.get("position")}),
                            editFields,
                            D.div({id: "footer"},
                                D.div({className: "footerleft formbuttons floating"}, [
                                    D.input({
                                        type: "button",
                                        className: "button mainaction",
                                        value: saveLabel,
                                        onClick: function (e) {
                                            e.preventDefault();
                                            saveCommand();
                                        }
                                    }),
                                    R.createElement(ui.RuleActiveField, {data: this.props.data.get('active')})
                                ])
                            )
                        ])
                    )
                ])
            );
        }
    });

    ui.EmptyRulePane = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div(null,
                    D.div({
                        id: "filter-box",
                        className: "uibox contentbox",
                        style: {left: "260px"}
                    }, getLabel('choosefiltertoedit'))
                )
            );
        }
    });

    ui.RulePane = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div(null, [
                    D.div({
                        id: "filter-box",
                        className: "uibox contentbox",
                        style: {left: "260px", overflowY: "scroll"}
                    }, R.createElement(ui.RuleEditForm, this.props))
                ])
            );
        }
    });

    ui.RulesView = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var currentRule = this.props.data.get("currentRule");
            var rulePane;
            if (currentRule.has("id")) {
                rulePane = ui.RulePane;
            } else {
                rulePane = ui.EmptyRulePane;
            }

            return (
                D.div(null, [
                    R.createElement(ui.RuleListPane, {
                        data: Immutable.fromJS({
                            rules: this.props.data.get("rules"),
                            selectedRule: this.props.data.get("selectedRule")
                        })
                    }),
                    R.createElement(rulePane, {data: this.props.data.get("currentRule")}),
                    R.createElement(ui.RuleListMenu, {
                        data: this.props.data.get('listMenu').set(
                            'currentRule', this.props.data.get('currentRule')
                        )
                    })
                ])
            );
        }
    });
})();

