<?php

class ispmanager_rule extends rcube_plugin
{
    public $task = 'mail|settings';

    private $rc;
    private $engine;
    private $ui_initialized = false;

    function init()
    {
        $this->rc = rcube::get_instance();

        // register actions
        $this->register_action('plugin.ispmanager_rule', array($this, 'rule_actions'));
        $this->register_action('plugin.ispmanager_rule-action', array($this, 'rule_actions'));

        $this->add_hook('settings_actions', array($this, 'settings_actions'));

        $this->init_ui();
    }

    /**
     * Initializes plugin's UI (localization, js script)
     */
    function init_ui()
    {
        if ($this->ui_initialized) {
            return;
        }

        // load localization
        $this->add_texts('localization/');

        $skin_path = $this->local_skin_path();

        $rule_action = strpos($this->rc->action, 'plugin.ispmanager_rule') === 0;

        if ($rule_action) {
            $this->include_script('react-0.13.3.min.js');
            $this->include_script('immutable.min.js');
            $this->include_script('ispmanager_rule.js');
            $this->include_script('ispmanager_rule_ui.js');
            // include skin-specific ui templates
            $this->include_script("$skin_path/ui.js");
            // include default localization
            $this->include_script('localization/en_US.js');

            $localizationDir = slashify(realpath(slashify($this->home) . "localization"));
            if (is_file($localizationDir.$_SESSION['language'].'.js')) {
                $this->include_script('localization/'.$_SESSION['language'].'.js');
            }
        }

        // include styles
        $this->include_stylesheet("$skin_path/ispmanager_rule.css");

        $this->ui_initialized = true;
    }

    /**
     * Adds Rules section in Settings
     */
    function settings_actions($args)
    {
        $this->load_config();

        // register Rules action
        $args['actions'][] = array(
            'action' => 'plugin.ispmanager_rule',
            'class' => 'filter',
            'label' => 'filters',
            'domain' => 'ispmanager_rule',
            'title' => 'filterstitle',
        );

        return $args;
    }

    /**
     * Plugin action handler
     */
    function rule_actions()
    {
        $engine = $this->get_engine();

        $this->init_ui();
        $engine->actions();
    }

    function get_engine()
    {
        if (!$this->engine) {
            $this->load_config();

            // Add include path for internal classes
            $include_path = $this->home . '/lib' . PATH_SEPARATOR;
            $include_path .= ini_get('include_path');
            set_include_path($include_path);

            $this->engine = new rcube_rule_engine($this);
        }

        return $this->engine;
    }
}
