ISPManager filter API plugin for Roundcube
==========================================

A plugin for managing filters using ISPSystem's (https://www.ispsystem.com/) ISPmanager API.

ATTENTION
---------

That plugin is still WORK IN PROGRESS and is not supposed to be used in production.

![Screen from that actual plugin](http://zoldar.net/ispmanager_filter.png)


Quick start
-----------

First, fetch the plugin to Roundcube's `plugins/` folder:

    git clone git@bitbucket.org:zoldar/roundcube_ispmanager_filter.git $ROUNDCUBE_HOME/plugins/ispmanager_rule

Next, copy `config.inc.php.dist` to `config.inc.php` and set proper ISPManager URL.

Finally, add `ispmanager_rule` to `$config['plugins']` array in the main config file under `$ROUNDCUBE_HOME/config/config.inc.php`.
    
License
-------
This plugin is released under the [GNU General Public License Version 3+](http://www.gnu.org/licenses/gpl.html).
